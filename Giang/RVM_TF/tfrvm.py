import tensorflow as tf
from utils import *
class TF_RVM:
    def __init__(self, n_iter=300,
                 tol=1e-3,
                 verbose=False,
                 kernel='rbf',
                 gamma=None,
                 degree=3,
                 coef0=1.):
        self.n_iter = n_iter
        self.tol = tol
        self.verbose = verbose
        self.kernel = kernel
        self.gamma = gamma
        self.degree = degree
        self.coef0 = coef0
        
    def fit(self, X, y):
        