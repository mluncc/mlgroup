import tensorflow as tf
import sys
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

def _euclidean_distances(X, Y=None):
    if Y is None:
        Y = tf.identity(X)
        
    XX = tf.reduce_sum(tf.square(X), axis=1, keepdims=True)
    YY = tf.reduce_sum(tf.square(Y), axis=1)
    XY = tf.matmul(X, Y, transpose_b=True)
    return XX + YY - 2*XY

def _linear_kernel(X, Y=None):
    if Y is None:
        Y = tf.identity(X)
        
    return tf.matmul(X, Y, transpose_b=True)

def _rbf_kernel(X, Y=None, 
                gamma=tf.constant(1.0, dtype=tf.float64)):
    if Y is None:
        Y = tf.identity(X)

    K = _euclidean_distances(X, Y)
    K *= -gamma
    K = tf.exp(K)
    return K

def _sigmoid_kernel(X, Y=None, 
                    gamma=tf.constant(1.0, dtype=tf.float64), 
                    coef0=tf.constant(1.0, dtype=tf.float64)):
    if Y is None:
        Y = tf.identity(X)
        
    K = tf.matmul(X, Y, transpose_b=True)
    K *= gamma
    K += coef0
    K = tf.tanh(K)
    return K

def _polynomial_kernel(X, Y=None, 
                       gamma=tf.constant(1.0, dtype=tf.float64), 
                       coef0=tf.constant(1.0, dtype=tf.float64), 
                       degree=tf.constant(3.0, dtype=tf.float64)):
    if Y is None:
        Y = tf.identity(X)
    
    K = tf.matmul(X, Y, transpose_b=True)
    K *= gamma
    K += coef0
    K **= degree
    return K
    
def _kernel_function(name):
    if name == 'linear':
        return linear_kernel
    elif name == 'rbf':
        return rbf_kernel
    elif name == 'poly':
        return polynomial_kernel
    elif name == 'sigmoid':
        return sigmoid_kernel
    else:
        print('Do not recognize kernel name!')
        sys.exit(0)
        
def get_session():
    """Returns recently made Tensorflow session"""
    return tf.get_default_session()

if __name__ == '__main__':
    import numpy as np
    from sklearn.metrics.pairwise import *
    
    sess = tf.InteractiveSession()
    x = np.random.randn(10,6).astype(np.float64)
    X = tf.placeholder(shape=[None, None], dtype=tf.float64)
    
    d = _euclidean_distances(X)
#     print('tf eucidean:\n', d.eval(feed_dict={X:x}))
#     print('sklearn euclidean:\n', euclidean_distances(x, squared=True))
    print('Euclidean Distance Equal:', np.allclose(d.eval(feed_dict={X:x}), euclidean_distances(x, squared=True)))
    
    k = _rbf_kernel(X, gamma=tf.constant(1.0/x.shape[1], dtype=tf.float64))
#     print('tf rbf_kernel:\n', k.eval(feed_dict={X:x}))
#     print('sklearn rbf kernel:\n', rbf_kernel(x))
    print('rbf kernel equal:', np.allclose(k.eval(feed_dict={X:x}), rbf_kernel(x)))
    
    k = _sigmoid_kernel(X, gamma=tf.constant(1.0/x.shape[1], dtype=tf.float64))
#     print('tf sigmoid kernel:\n', k.eval(feed_dict={X:x}))
#     print('sklearn sigmoid kernel:\n', sigmoid_kernel(x))
    print('sigmoid kernel equal:', np.allclose(k.eval(feed_dict={X:x}), sigmoid_kernel(x)))
    
    k = _polynomial_kernel(X, gamma=tf.constant(1.0/x.shape[1], dtype=tf.float64))
#     print('tf polynomial kernel:\n', k.eval(feed_dict={X:x}))
#     print('sklearn polynomial kernel:\n', polynomial_kernel(x))
    print('polynomial kernel equal:', np.allclose(k.eval(feed_dict={X:x}), polynomial_kernel(x)))
    
    sess.close()