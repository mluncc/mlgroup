import tensorflow as tf
from collections import deque
import numpy as np
import random
import gym
import matplotlib.pyplot as plt
plt.ion()

random.seed(911)
np.random.seed(911)
tf.set_random_seed(911)

from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.optimizers import Adam

sample_env = gym.make('CartPole-v0')
OBSERVATIONS_DIM = sample_env.observation_space.shape[0]
ACTIONS_DIM = sample_env.action_space.n

MAX_ITERATIONS = 201
LEARNING_RATE = 1e-3
GAMMA = 0.99
REPLAY_MEMORY_SIZE = 200*100
NUM_EPISODES = 1000000
# TARGET_UPDATE_FREQ = 100
MINIBATCH_SIZE = 32
RANDOM_ACTION_DECAY = 0.99
INITIAL_RANDOM_ACTION = 1.0
    
class Agent():
    @staticmethod
    def _get_model():
        model = Sequential()
        model.add(Dense(16, input_shape=(OBSERVATIONS_DIM, ), activation='relu'))
        model.add(Dense(16, activation='relu'))
        model.add(Dense(ACTIONS_DIM, activation='linear'))
        model.compile(optimizer=Adam(lr=LEARNING_RATE),
                      loss='mse',
                      metrics=[]
                     )
        return model
    
    def __init__(self):
        self.memory = deque([], maxlen=REPLAY_MEMORY_SIZE)
        self.model = Agent._get_model()
        self.eps = INITIAL_RANDOM_ACTION
        self.eps_decay_factor = RANDOM_ACTION_DECAY
        self.env = gym.make('CartPole-v0')
#         self.env._max_episode_steps = 201
        
    def freeze(self):
        self.model.layers[0].trainable = False
        self.model.layers[1].trainable = False
        self.model.compile(optimizer=Adam(lr=LEARNING_RATE), 
                           loss='mse',
                           metrics=[]
                          )
        
    def predict(self, obs):
        np_obs = np.reshape(obs, [-1, OBSERVATIONS_DIM])
        return self.model.predict(np_obs)
    
    def model_fit(self, obss, targets):
        np_obss = np.reshape(obss, [-1, OBSERVATIONS_DIM])
        np_targets = np.reshape(targets, [-1, ACTIONS_DIM])
        self.model.fit(np_obss, np_targets, epochs=1, verbose=0)
        
    def choose_action(self, obs):
        if np.random.random() < self.eps:
            action = np.random.choice(range(ACTIONS_DIM))
        else:
            action = np.argmax(self.predict(obs))
            
        self.eps = max(self.eps*self.eps_decay_factor, 0.1)
        return action
    
    def train(self, samples):
        random.shuffle(samples)
        obss = []
        targets = []
        for sample in samples:
            old_obs, action, reward, obs = sample
            target = np.reshape(self.predict(old_obs), ACTIONS_DIM)
            target[action] = reward
            if obs is not None:
                pred = self.predict(obs)
                new_action = np.argmax(pred)
                target[action] += GAMMA * pred[0, new_action]
                
            obss.append(old_obs)
            targets.append(target)
            
        self.model_fit(obss, targets)
    
    def main(self):
        total_rewards = deque([], maxlen=100)
        rewards = []
        cnt = 0
        for e in range(NUM_EPISODES):
            obs = self.env.reset()
            i_rewards = 0
            
            for i in range(MAX_ITERATIONS):
                old_obs = obs
                action = self.choose_action(obs)
                obs, reward, done, info = self.env.step(action)
                
                i_rewards += reward
                if done:
                    print('Episode {} \t Iteration {} \t Eps {}'. format(e, i, self.eps))
                    plt.scatter(cnt, i)
                    plt.pause(0.05)
                    total_rewards.append(i_rewards+1)
                    rewards.append(i_rewards+1)
                    
                    if i < 199:
                        reward = -1
                        self.memory.append((old_obs, action, reward, None))
                    else:
                        self.memory.append((old_obs, action, reward, obs))
                        
#                     if e >= 100 and sum(total_rewards)/len(total_rewards) >= 195.0:
#                         print('Problem Solve at episode {} average {} std {}'.format(e-100, 
#                                                                                      np.mean(total_rewards), 
#                                                                                      np.std(total_rewards)))
                        
                    break
                    
                self.memory.append((old_obs, action, reward, obs))
                
                if len(self.memory) >= MINIBATCH_SIZE:
                    train_samples = random.sample(self.memory, MINIBATCH_SIZE)
                    self.train(train_samples)
                    cnt += 1
                    
if __name__ == '__main__':
    agent = Agent()
    agent.main()
        