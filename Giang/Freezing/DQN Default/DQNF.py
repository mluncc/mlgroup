import tensorflow as tf
from collections import deque
import numpy as np
import random
import gym
from twilio.rest import Client

random.seed(911)
np.random.seed(911)
tf.set_random_seed(911)

from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import Dense, Input
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras import backend as K

sample_env = gym.make('CartPole-v0')
OBSERVATIONS_DIM = sample_env.observation_space.shape[0]
ACTIONS_DIM = sample_env.action_space.n

MAX_ITERATIONS = 201
LEARNING_RATE = 1e-3
GAMMA = 0.99
REPLAY_MEMORY_SIZE = 200*100
NUM_EPISODES = 10000
# TARGET_UPDATE_FREQ = 100
MINIBATCH_SIZE = 32
RANDOM_ACTION_DECAY = 0.99
INITIAL_RANDOM_ACTION = 1.0
VERBOSE = 0

class Agent():
    def _get_model(self):
        self.x = Input(shape=(OBSERVATIONS_DIM, ))
        self.layer1 = Dense(16, activation='relu')
        self.h1 = self.layer1(self.x)
        self.layer2 = Dense(16, activation='relu')
        self.h2 = self.layer2(self.h1)
        self.layer3 = Dense(ACTIONS_DIM, activation='linear')
        self.y = self.layer3(self.h2)
        self.model = Model(self.x, self.y)
        self.model.compile(optimizer=Adam(lr=LEARNING_RATE),
                           loss='mse',
                           metrics=[]
                          )

    def freeze(self):
        # Freeze layer 1 and 2
        self.layer1.trainable = False
        self.layer2.trainable = False
        self.model.compile(optimizer=Adam(lr=LEARNING_RATE),
                           loss='mse',
                           metrics=[]
                          )

    def __init__(self, freeze_point):
        self.memory = deque([], maxlen=REPLAY_MEMORY_SIZE)
        self._get_model()
        self.eps = INITIAL_RANDOM_ACTION
        self.eps_decay_factor = RANDOM_ACTION_DECAY
        self.env = gym.make('CartPole-v0')
        self.verbose = VERBOSE
        self.freeze_point = freeze_point

    def predict(self, obs):
        np_obs = np.reshape(obs, [-1, OBSERVATIONS_DIM])
        return self.model.predict(np_obs)

    def model_fit(self, obss, targets):
        np_obss = np.reshape(obss, [-1, OBSERVATIONS_DIM])
        np_targets = np.reshape(targets, [-1, ACTIONS_DIM])
        self.model.fit(np_obss, np_targets, epochs=1, verbose=0)

    def choose_action(self, obs):
        if np.random.random() < self.eps:
            action = np.random.choice(range(ACTIONS_DIM))
        else:
            action = np.argmax(self.predict(obs))

        self.eps = max(self.eps*self.eps_decay_factor, 0.1)
        return action

    def train(self, samples):
        random.shuffle(samples)
        obss = []
        targets = []
        for sample in samples:
            old_obs, action, reward, obs = sample
            target = np.reshape(self.predict(old_obs), ACTIONS_DIM)
            target[action] = reward
            if obs is not None:
                pred = self.predict(obs)
                new_action = np.argmax(pred)
                target[action] += GAMMA * pred[0, new_action]

            obss.append(old_obs)
            targets.append(target)

        self.model_fit(obss, targets)

    def main(self, cnt):
        total_rewards = deque([], maxlen=100)
        rewards = []
        for e in range(NUM_EPISODES):
            obs = self.env.reset()
            i_rewards = 0

            if e == self.freeze_point:
                self.freeze()

            for i in range(MAX_ITERATIONS):
                old_obs = obs
                action = self.choose_action(obs)
                obs, reward, done, info = self.env.step(action)

                i_rewards += reward
                if done:
                    if self.verbose:
                        print('Episode {} \t Iteration {}'. format(e, i))
                    total_rewards.append(i_rewards)
                    rewards.append(i_rewards)

                    if i < 199:
                        reward = -200
                        self.memory.append((old_obs, action, reward, None))
                    else: # CartPole ends after 200 steps
                        self.memory.append((old_obs, action, reward, obs))

                    if e >= 100 and sum(total_rewards)/len(total_rewards) >= 195.0:
                        print('{} Problem Solve at episode {} average {} std {}'.format(cnt, e-100,
                                                                                     np.mean(total_rewards),
                                                                                     np.std(total_rewards)))
                        
                    break

                self.memory.append((old_obs, action, reward, obs))

                if len(self.memory) >= MINIBATCH_SIZE:
                    train_samples = random.sample(self.memory, MINIBATCH_SIZE)
                    self.train(train_samples)

    def test_freeze(self):
        for i in range(len(self.model.layers)):
            try:
                w.append([])
                w[i-1].append(self.model.layers[i].get_weights())
            except:
                w = []

        total_rewards = deque([], maxlen=100)
        obs = self.env.reset()
        for i in range(MAX_ITERATIONS):
            old_obs = obs
            action = self.choose_action(obs)
            obs, reward, done, info = self.env.step(action)

            self.memory.append((old_obs, action, reward, obs))
            if len(self.memory) >= 1:
                train_samples = random.sample(self.memory, 1)
                if i == 1:
                    self.freeze()
                self.train(train_samples)
                for j in range(1, len(self.model.layers)):
                    w[j-1].append(self.model.layers[j].get_weights())
                if i == 1:
                    return w

if __name__ == '__main__':
    agent = Agent(NUM_EPISODES)
    agent.main(0)

# if __name__ == '__main__':
#     agent = Agent(2)
#     w = agent.test_freeze()
#     for i in range(len(w)):
#         for j in range(1, len(w[i])):
            # Check weights and bias if they equal the previous one
#             print(i, j, 'Weights Equal:', np.array_equal(w[i][j-1][0], w[i][j][0]), 'Bias Equal:', np.array_equal(w[i][j-1][1], w[i][j][1]))
