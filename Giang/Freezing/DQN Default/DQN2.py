# -*- coding: utf-8 -*-
import random
import gym
import numpy as np
from collections import deque
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.optimizers import Adam, SGD
from tensorflow.python.keras import backend as K

EPISODES = 1000000
plt.ion()

class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=50000)
        self.gamma = 0.99    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.02
        self.epsilon_decay = 0.99
        self.learning_rate = 1e-3
        self.learning_starts = 1000

        sess = tf.InteractiveSession()
        K.set_session(sess)
        self.model = self._build_model()

    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model.add(Dense(64, input_dim=self.state_size, activation='relu'))
        # model.add(Dense(16, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse',
                      optimizer=Adam(lr=self.learning_rate))
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
                target = (reward + self.gamma *
                          np.amax(self.model.predict(next_state)[0]))
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay


if __name__ == "__main__":
    env = gym.make('CartPole-v0')
    state_size = env.observation_space.shape[0]
    action_size = env.action_space.n
    agent = DQNAgent(state_size, action_size)
    done = False
    batch_size = 32

    state = env.reset()
    episode_rewards = [0.0]
    
    old_weights = []
    new_weights = []
    change_w = []
    change_b = []
    for jj in range(len(agent.model.layers)):
        old_weights.append(agent.model.layers[jj].get_weights())
        new_weights.append(agent.model.layers[jj].get_weights())
        change_w.append(K.eval(K.sum(K.abs(new_weights[jj][0] - old_weights[jj][0]))))
        change_b.append(K.eval(K.sum(K.abs(new_weights[jj][1] - old_weights[jj][1]))))
        
#     print(change_w) # [0.0, 0.0]
#     print(change_b) # [0.0, 0.0]
    
    for e in range(EPISODES):
        state = np.reshape(state, [1, state_size])
        action = agent.act(state)
        next_state, reward, done, _ = env.step(action)
        next_state = np.reshape(next_state, [1, state_size])
        episode_rewards[-1] += reward
        reward = reward if not done or episode_rewards[-1] == 200 else -1.0 # Clipping reward
        agent.remember(state, action, reward, next_state, done)
        state = next_state
        
        if done:
            state = env.reset()
            mean_100ep_reward = round(np.mean(episode_rewards[-101:-1]), 1)
            num_episodes = len(episode_rewards)
            print(e, num_episodes, mean_100ep_reward, episode_rewards[-1])
            episode_rewards.append(0.0)
            if mean_100ep_reward > 195.0:
                print('Converged')
                break
            if e > agent.learning_starts:
                agent.replay(batch_size)
                for jj in range(len(agent.model.layers)):
                    new_weights[jj] = agent.model.layers[jj].get_weights()
                    change_w[jj] = K.eval(K.sum(K.abs(new_weights[jj][0] - old_weights[jj][0])))
                    change_b[jj] = K.eval(K.sum(K.abs(new_weights[jj][1] - old_weights[jj][1])))
                print('Weights and Bias change:', change_w, change_b)                
                
                # Update old_weights
                old_weights = np.copy(new_weights)

    # for e in range(EPISODES):
    #     state = env.reset()
    #     state = np.reshape(state, [1, state_size])
    #     for time in range(200):
    #         # env.render()
    #         action = agent.act(state)
    #         next_state, reward, done, _ = env.step(action)
    #         reward = reward if not done else -1
    #         next_state = np.reshape(next_state, [1, state_size])
    #         agent.remember(state, action, reward, next_state, done)
    #         state = next_state
    #         if done:
    #             print("episode: {}/{}, score: {}, e: {:.2}"
    #                   .format(e, EPISODES, time, agent.epsilon))
    #             plt.scatter(e, time)
    #             plt.pause(0.05)
    #             break
    #         if len(agent.memory) > batch_size:
    # #             for jj in range(len(agent.model.layers)):
    # #                 old_weights.append(agent.model.layers[jj].get_weights())
    #             agent.replay(batch_size)
    #
    # K.clear_session()
