# Inspired by: https://github.com/AmazaspShumik/sklearn-bayes
import numpy as np
from scipy.linalg import solve_triangular
from numpy.linalg import LinAlgError
from scipy.linalg import pinv
from utils import *

class RelevantVectorRegression:
    def __init__(self, 
                 n_iter=300, 
                 tol=1e-3, 
                 verbose=False, 
                 kernel='rbf', 
                 gamma=None, 
                 degree=3, 
                 coef0=1):
        self.n_iter = n_iter
        self.tol = tol
        self.verbose = verbose
        self.kernel = kernel
        self.gamma = gamma
        self.degree = degree
        self.coef0 = coef0
        
    def _fit(self, X, y):
        n_samples, n_features = X.shape
        
        XY = np.dot(X.T, y)
        XX = np.dot(X.T, X)
        XXd = np.diag(XX)
        var_y = np.var(y)
        
        if var_y == 0:
            beta = 1e-2
        else:
            beta = 1./var_y
            
        A = np.PINF*np.ones(n_features)
        active = np.zeros(n_features, dtype=np.bool)
        
        # Initialize A and active index
        if np.sum(XXd < np.finfo(np.float32).eps) > 0: # If there is too small value XX diagonal --> start with index 0
            A[0] = np.finfo(np.float32).eps
            active[0] = True
        else:
            proj = XY**2 / XXd
            start = np.argmax(proj)
            active[start] = True
            A[start] = XXd[start]/(proj[start] - var_y)
            
        for i in range(self.n_iter):
#         while True:
            XXa = XX[active, :][:, active]
            XYa = XY[active]
            Aa = A[active]
            
            Mn, Ri, cholesky = self._posterior_distribution(Aa, beta, XXa, XYa)
            if cholesky:
                Sdiag = np.sum(Ri**2, axis=0)
            else:
                Sdiag = np.copy(np.diag(Ri))
                
            s, q, S, Q = self._sparsity_and_quality(XX, XXd, XY, XYa, Aa, Ri, active, beta, cholesky)
            
            # Update beta
            rss = np.sum((y - np.dot(X[:,active], Mn))**2)
            beta = (n_samples - np.sum(active) + np.sum(Aa*Sdiag)) / (rss + np.finfo(np.float32).eps)
            
            A, converged, active = self._update_precisions(Q, S, q, s, A, active, self.tol, n_samples, True)
            
            if self.verbose:
                print('Iteration: {0}, number of features in the model: {1}'.format(i, np.sum(active)))
            if converged:
                if self.verbose:
                    print('Converged')
                break

        XXa, XYa, Aa = XX[active,:][:,active], XY[active], A[active]
        Mn, Ri, cholesky = self._posterior_distribution(Aa, beta, XXa, XYa, True)
        self.coef_ = np.zeros(n_features)
        self.coef_[active] = Mn
        self.sigma_ = Ri
        self.active_ = active
        self.lambda_ = A
        self.alpha_ = beta
            
    def _posterior_distribution(self, A, beta, XX, XY, full_covariance=False):
        Sinv = beta*XX
        np.fill_diagonal(Sinv, np.diag(Sinv) + A)
        cholesky = True
        try:
            # mean = beta*(Sinv)^-1*X.T*Y
            # Sinv*mean = beta*XY
            # If Sinv is cholesky (Sinv = Sinv.T) --> cholesky decomposing to R --> faster computation
            # R*R.T*mean = beta*XY
            R = np.linalg.cholesky(Sinv)
            Rt_Mn = solve_triangular(R, beta*XY, check_finite=False, lower=True)
            Mn = solve_triangular(R.T, Rt_Mn, check_finite=False, lower=False)
            
            Rinv = solve_triangular(R, np.eye(A.shape[0]), check_finite=False, lower=True)
            if full_covariance:
                Ri = np.dot(Rinv.T, Rinv)
                return Mn, Ri, cholesky
            else:
                return Mn, Rinv, cholesky
        except LinAlgError:
            cholesky = False
            Rinv = pinv(Sinv)
            Mn = beta*np.dot(Sinv, XY)
            return Mn, Rinv, cholesky
        
    def _sparsity_and_quality(self, XX, XXd, XY, XYa, Aa, Ri, active, beta, cholesky):
        bXY = beta*XY
        bXX = beta*XXd
        if cholesky:
            XXR = np.dot(XX[:,active], Ri.T)
            RXY = np.dot(Ri, XYa)
            S = bXX - beta**2 * np.sum(XXR**2, axis=1)
            Q = bXY - beta**2 * np.dot(XXR, RXY)
        else:
            XXa = XX[:,active]
            XS = np.dot(XXa, Ri)
            S = bXX - beta**2 * np.sum(XS*XXa, axis=1)
            Q = bXY - beta**2 * np.dot(XS, XYa)
        
        qi = np.copy(Q)
        si = np.copy(S)
        Qa, Sa = Q[active], S[active]
        qi[active] = Aa*Qa/(Aa - Sa)
        si[active] = Aa*Sa/(Aa - Sa)
        return si, qi, S, Q
    
    def _update_precisions(self, Q, S, q, s, A, active, tol, n_samples, clf_bias):
        deltaL = np.zeros(Q.shape[0])

        theta = q**2 - s
        add = (theta > 0) * (active == False)
        recompute = (theta > 0) * (active == True)
        delete = ~(add + recompute)

        Qadd, Sadd = Q[add], S[add]
        Qrec, Srec, Arec = Q[recompute], S[recompute], A[recompute]
        Qdel, Sdel, Adel = Q[delete], S[delete], A[delete]

        Anew = s[recompute]**2 / (theta[recompute] + np.finfo(np.float32).eps)
        delta_alpha = (1./Anew - 1./Arec)

        deltaL[add] = (Qadd**2 - Sadd)/Sadd + np.log(Sadd/Qadd**2)
        deltaL[recompute] = Qrec**2/(Srec + 1./delta_alpha) - np.log(1 + Srec*delta_alpha)
        deltaL[delete] = Qdel**2/(Sdel - Adel) - np.log(1 - Sdel/Adel)
        deltaL = deltaL/n_samples

        feature_index = np.argmax(deltaL)
        same_features = np.sum(theta[~recompute] > 0) == 0
        no_delta = np.sum(abs(Anew - Arec) > tol) == 0

        converged = False
        if same_features and no_delta:
            converged = True
            return A, converged, active

        if theta[feature_index] > 0:
            A[feature_index] = s[feature_index]**2 / theta[feature_index]
            if active[feature_index] == False:
                active[feature_index] = True
        else:
            if active[feature_index] == True and np.sum(active) >= 2:
                if not (feature_index == 0 and clf_bias):
                    active[feature_index] = False
                    A[feature_index] = np.PINF
        return A, converged, active
        
    def fit(self, X, y, relevant_vectors=None, relevant_ys=None):
        X = X.astype(np.float64)
        y = y.astype(np.float64)
        
        assert X.shape[0] == y.shape[0], 'Does not have same  number of elements between X and y'
        if relevant_vectors is not None:
            assert X.shape[1] == relevant_vectors.shape[1], 'Does not have same number of dimensions between X and relevant_vectors'
            assert relevant_vectors.shape[0] == relevant_ys.shape[0], 'Does not have same number of elements between relevant_vectors and relevant_ys'
            # Stack X with relevant_vectors and y with relevant_ys
            X = np.vstack((relevant_vectors, X))
            y = np.vstack((relevant_ys, y))
            
        self.kernelf = kernel_function(self.kernel)
        K = self.kernelf(X)
        self._fit(K, y)
        
        self.relevant_ = np.where(self.active_ == True)[0]
        if X.ndim == 1:
            self.relevant_vectors_ = X[self.relevant_]
        else:
            self.relevant_vectors_ = X[self.relevant_, :]
            
        print('Number of Relevant Vectors:', self.relevant_vectors_.shape[0])
        return self
        
    def _kernel_decision_function(self, X):
        K = self.kernelf(X, Y=self.relevant_vectors_)
            
        return K, np.dot(K, self.coef_[self.active_])
        
    def predict_distribution(self, X):
        K, y_hat = self._kernel_decision_function(X)
        var_hat = 1./self.alpha_ + np.sum(np.dot(K, self.sigma_)*K, axis=1)
        return y_hat, var_hat
