import numpy as np

def euclidean_distances(X, Y=None):
    """
    Compute Euclidean Distance: ||X - Y||^2
    """
    if Y is None:
        Y = np.copy(X)
        
    XX = np.sum(X**2, axis=1, keepdims=True)
    YY = np.sum(Y**2, axis=1)
    XY = np.dot(X, Y.T)
    return XX + YY - 2*XY

def linear_kernel(X, Y=None, gamma=None, coef0=1., degree=3):
    """
    Compute Linear kernel of X and Y: <X, Y>
    """
    if Y is None:
        Y = np.copy(X)
    return np.dot(X, Y.T)

def rbf_kernel(X, Y=None, gamma=None, coef0=1., degree=3):
    """
    Compute RBF (Gaussian) kernel of X and Y: e^(-gamma*||X - Y||^2)
    """
    if Y is None:
        Y = np.copy(X)
        
    if gamma is None:
        gamma = 1.0/X.shape[1]
        
    K = euclidean_distances(X, Y)
    K *= -gamma
    np.exp(K, K)
    return K

def sigmoid_kernel(X, Y=None, gamma=None, coef0=1., degree=3):
    """
    Compute Sigmoid kernel of X and Y: K(X, Y) = tanh(gamma <X,Y> + coef0)
    """
    if Y is None:
        Y = np.copy(X)
    
    if gamma is None:
        gamma = 1.0/X.shape[1]
    
    K = np.dot(X, Y.T)
    K *= gamma
    K += coef0
    np.tanh(K, K)
    return K

def polynomial_kernel(X, Y=None, gamma=None, coef0=1., degree=3):
    """
    Compute Polynomial kernel of X and Y: K(X, Y) = (gamma <X, Y> + coef0)^degree
    """
    if Y is None:
        Y = np.copy(X)
    if gamma is None:
        gamma = 1.0/X.shape[1]
        
    K = np.dot(X, Y.T)
    K *= gamma
    K += coef0
    K **= degree
    return K

def kernel_function(name):
    if name == 'linear':
        return linear_kernel
    elif name == 'rbf':
        return rbf_kernel
    elif name == 'poly':
        return polynomial_kernel
    elif name == 'sigmoid':
        return sigmoid_kernel
    else:
        print('Do not recognize kernel name!')
        