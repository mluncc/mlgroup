import numpy as np
import random
from RLRVM import RLRVM
from collections import deque
import gym
from utils import kernel_function
import sys

class Agent:
    def __init__(self, 
                 env_name='CartPole-v0', 
                 kernel='rbf', 
                 max_iters=1000000, 
                 k=0.03): 
        self.env = gym.make(env_name)
        self.num_action =  self.env.action_space.n
#         self.num_state = self.env.observation_space.shape[0]
        self.max_iters = max_iters
        
        # Model
        self.kernelf = kernel_function(kernel)
        self.rvs_states = np.array([])
        self.rvs_actions = np.array([])
        self.rvs_rewards = np.array([])
        self.rvs_weights = np.array([])
        self.weight_sensitive = k
        
        # Game specific
        self.eps = 1.0
        self.eps_decay = 0.99
        self.eps_min = 0.02
        self.gamma = 0.99
        
    def action_decoded(self, a):
        """
        Decode action to relative value
        """
        if 'CartPole-v0' in str(self.env.spec):
            return -1. if a == 0 else 1.
        
    def predict(self, state, action):
        """
        Predict q value of state and action
        """
        if self.rvs_states.shape[0] > 0:
            try:
                K_state = self.kernelf(state, Y=self.rvs_states)
            except IndexError:
                state = state.reshape((1, -1))
                K_state = self.kernelf(state, Y=self.rvs_states)
            try:
                K_action = self.kernelf(action, Y=self.rvs_actions)
            except AttributeError:
                action = np.array(action, dtype=np.float64).reshape((1,-1))
                K_action = self.kernelf(action, Y=self.rvs_actions)
            K = K_state * K_action
            
            return np.dot(K, self.rvs_weights)[0]
        else:
            return np.random.randn()
        
    def act(self, state):
        """
        Return action and Q value based on current state
        """
        if np.random.randn() < self.eps:
            a = np.random.choice(self.num_action)
            q_val = self.predict(state, self.action_decoded(a))
        else:
            q = []
            for i in range(self.num_action):
                q.append(self.predict(state, self.action_decoded(i)))
            a = np.argmax(q)
            q_val = max(q)
        return a, q_val

    def prep_data(self, e_memory):
        """
        Prepare data for training:
            Evaluate target Q value of new input = reward + gamma*max_Q(next_s, all_a)
            Re-evaluate target Q value of rvs input = reward + gamma*max_Q(s, all_a) -- might consider next_s for s
            Stack them all together
        """
        states = []
        actions = []
        rewards = []
        target_qs = []
            
        for i in range(len(e_memory)):
            """
            s1, a1, r1, s2, q1, target_q1 = r1 + gamma*q2
            s2, a2, r2, s3, q2, target_q2 = r2 + gamma*q3
            s3, a3, r3, s4, q3, target_q3 = r3 + gamma*q4 -- Need to evaluate q4 base on s4
            """
            state, action, _, reward, next_state, done = e_memory[i]
            try:
                _, _, q_val, _, _, _ = e_memory[i+1]
            except IndexError:
                q = []
                for i in range(self.num_action):
                    q.append(self.predict(next_state, self.action_decoded(i)))
                q_val = np.max(q)
            target_q = reward + (1.0-float(done))*self.gamma*q_val # Evaluate target Q
#             print(reward, target_q, q_val)
            # Append to training lists
            states.append(state.tolist())
            actions.append(action if type(action) == list else [action])
            rewards.append([reward])
            target_qs.append(target_q)
            
        if self.rvs_states.shape[0] > 0:
            assert self.rvs_states.shape[0] == self.rvs_actions.shape[0], 'rvs states and actions shape does not match'
            for i in range(len(self.rvs_states)):
                if self.rvs_states[i].tolist() in states and self.rvs_actions[i].tolist() in actions:
                    # If already exists in training lists --> continue to remove redundant
                    continue
                
                q = []
                for j in range(self.num_action):
                    q.append(self.predict(state, self.action_decoded(j)))
                target_q = self.rvs_rewards[i][0] + (1.0 - float(done))*self.gamma*np.max(q)
#                 print(target_q)
                # Append rvs to training lists
                states.append(self.rvs_states[i].tolist())
                actions.append(self.rvs_actions[i].tolist())
                rewards.append(self.rvs_rewards[i].tolist())
                target_qs.append(target_q)
#         print(np.array(target_qs))
        return np.array(states), np.array(actions), np.array(rewards), np.array(target_qs)
    
    def train(self, states, actions, target_qs):
        """
        Train and return RVM based Reinforcement Learning model
        """
        model = RLRVM()
        model.fit(states, actions, target_qs)
        return model
    
    def handle_rvs(self, model, states, actions, rewards):
        """
        Handling relevant vectors after a training session
        """
        rvs_states_new = states[model.relevant_, :]
        rvs_actions_new = actions[model.relevant_, :]
        rvs_rewards_new = rewards[model.relevant_, :]
        weights_new = model.coef_[model.active_]

        if self.rvs_states.shape[0] > 0:
            # What about making k (weight_sensitive) tied to number of new training session rvs?
            # w = (1-k)*w + k*new_s
            self.rvs_weights = (1.0-self.weight_sensitive)*self.rvs_weights
            
            N_rvs = self.rvs_states.shape[0]
            rvs_s_a_old = np.hstack((self.rvs_states, self.rvs_actions))
            rvs_s_a_new = np.hstack((rvs_states_new, rvs_actions_new))
            rvs_s_a = np.vstack((rvs_s_a_old, rvs_s_a_new))
            
            for cnt, rvs in enumerate(rvs_s_a_new): # Check each rvs
                ii = np.where(np.all((rvs_s_a_old - rvs) == 0, axis=1))
                if ii[0].size > 0: # rvs re-appear in new training session
                    self.update_weights(ii[0][0], weights_new[cnt])
                else: # Brand new rvs
                    self.append_rvs(rvs_states_new[cnt], rvs_actions_new[cnt], rvs_rewards_new[cnt], weights_new[cnt])

        else:
            self.rvs_states = rvs_states_new
            self.rvs_actions = rvs_actions_new
            self.rvs_rewards = rvs_rewards_new
            self.rvs_weights = self.weight_sensitive*weights_new

        # Delete rvs which have weight < eps
        ii = np.where(np.abs(self.rvs_weights) < np.finfo(float).eps)
        self.pop_rvs(ii)
    
    def append_rvs(self, state, action, reward, weight):
        """
        Append brand new rvs to rvs storage
            Capable of append multiple values of state, action, reward, weight
        """
        self.rvs_states = np.vstack((self.rvs_states, state))
        self.rvs_actions = np.vstack((self.rvs_actions, action))
        self.rvs_rewards = np.vstack((self.rvs_rewards, reward))
        self.rvs_weights = np.append(self.rvs_weights, self.weight_sensitive*weight)
        
    def pop_rvs(self, idx):
        """
        Delete rvs states, actions, rewards, and weights of weights that are closed to 0
            Capable to pop multiple values if 'idx' is a list
        """
        self.rvs_states = np.delete(self.rvs_states, idx, 0)
        self.rvs_actions = np.delete(self.rvs_actions, idx, 0)
        self.rvs_rewards = np.delete(self.rvs_rewards, idx, 0)
        self.rvs_weights = np.delete(self.rvs_weights, idx, 0)
    
    def update_weights(self, idx, weight):
        """
        Update weights of re-appear rvs
            Capable to pop multiple values if 'idx' is a list, and weight is same order
        """
        self.rvs_weights[idx] += self.weight_sensitive*weight
    
    def log_message(self, n_ep, current_ep_reward, mean_100_ep, num_rvs):
        print('Episode: {}\t |Episode Reward: {}\t |Mean 100 Episode Rewards: {:.2f}\t |Num RVs {}'
              .format(n_ep, current_ep_reward, mean_100_ep, num_rvs))
    
    def main(self):
        state = self.env.reset()
        e_rewards = [0.0]
        e_memory = [] # Temporal episode memory
        for i in range(self.max_iters):
            action, q_val = self.act(state)
            next_state, reward, done, _ = self.env.step(action)
            
            e_rewards[-1] += reward # Increse episode reward
            
#             reward = reward if not done or e_rewards[-1] == 200 else -10 # Prep reward
            e_memory.append((state, self.action_decoded(action), q_val, reward, next_state, done))
            state = next_state
            
            if done:
                states, actions, rewards, target_qs = self.prep_data(e_memory)
                model = self.train(states, actions, target_qs)
                self.handle_rvs(model, states, actions, rewards)
                
                self.log_message(len(e_rewards), e_rewards[-1], np.mean(e_rewards[-100:]), self.rvs_states.shape[0])
                
                if np.mean(e_rewards[-100:]) >= 195.0: # Problem solved
                    print('Solved at {}'.format(len(e_rewards)))
                    break
                    
                e_memory = [] # Clear temporal memory
                self.eps = max(self.eps*self.eps_decay, self.eps_min) # Update greedy epsilon
                state = self.env.reset() # Reset environment
                e_rewards.append(0.0) # Append new episode reward
        
if __name__ == '__main__':
    agent = Agent()
    agent.main()