import numpy as np
from rvm import RelevantVectorRegression
from utils import *
        
class RLRVM(RelevantVectorRegression): # RLRVM is inherited from RelevantVectorRegression base
    def __init__(self, 
                 n_iter=1000, 
                 tol=1e-3, 
                 verbose=False, 
                 kernel='rbf', 
                 gamma=None, 
                 degree=3, 
                 coef0=1.0, 
                 combine=False):
        self.n_iter = n_iter
        self.tol = tol
        self.verbose = verbose
        self.kernel = kernel
        self.gamma = gamma
        self.degree = degree
        self.coef0 = coef0
        self.combine = combine
        
    def fit(self, states, actions, target_qs):
        states = states.astype(np.float64)
        actions = actions.astype(np.float64)
#         print(target_qs)
        target_qs = target_qs.astype(np.float64)
            
        self.kernelf = kernel_function(self.kernel)
        if self.combine:
            X = np.hstack((states, actions))
            K = self.kernelf(X)
        else:
            K_state = self.kernelf(states)
            K_action = self.kernelf(actions)
            K = K_state * K_action
        self._fit(K, target_qs)
        
        self.relevant_ = np.where(self.active_ == True)[0]
        if states.ndim == 1:
            self.relevant_vectors_state_ = states[self.relevant_]
        else:
            self.relevant_vectors_state_ = states[self.relevant_, :]
        if actions.ndim == 1:
            self.relevant_vectors_action_ = actions[self.relevant_]
        else:
            self.relevant_vectors_action_ = actions[self.relevant_, :]
        
        if self.verbose:
            print('Number of Relevant Vectors:', self.relevant_vectors_state_.shape[0])
        
    def _kernel_decision_function(self, state, action):
        K_state = self.kernelf(state, Y=self.relevant_vectors_state_)
        K_action = self.kernelf(action, Y=self.relevant_vectors_action_)
        K = K_state * K_action
            
        return K, np.dot(K, self.coef_[self.active_])
        
    def predict_distribution(self, state, action):
        K, y_hat = self._kernel_decision_function(state, action)
        var_hat = 1./self.alpha_ + np.sum(np.dot(K, self.sigma_)*K, axis=1)
        return y_hat, var_hat
    
if __name__ == '__main__':
    np.random.seed(1)
    state = np.random.randn(1000,4)
    action = np.random.randn(1000,1)
    q_value = state[:,0] + 2*state[:,1] + 3*state[:,2] + 4*state[:,3] + 5*action[:,0]
    relevant_states = np.random.randn(500,4)
    relevant_actions = np.random.randn(500,1)
    relevant_qs = relevant_states[:,0] + 2*relevant_states[:,1] + 3*relevant_states[:,2] + 4*relevant_states[:,3] + 5*relevant_actions[:,0]
    states = np.vstack((state, relevant_states))
    actions = np.vstack((action, relevant_actions))
    target_qs = np.append(q_value, relevant_qs)
#     print(state, '\n', action, '\n', q_value, '\n', relevant_states, '\n', relevant_actions, '\n', relevant_qs)
    rlrvm = RLRVM()
    rlrvm.fit(states, actions, target_qs)
    y_hat, y_var = rlrvm.predict_distribution(state=np.array([[0.1,0.2,0.3,0.4]]), action=np.array([[0.2]]))
    print('predict y:', y_hat)
    print('true y:', 0.1 + 2*0.2 + 3*0.4 + 4*0.4 + 5*0.2)