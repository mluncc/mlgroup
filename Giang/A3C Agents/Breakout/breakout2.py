import tensorflow as tf
import numpy as np
from tqdm import tqdm
from collections import deque
from Giang_TF_Helpers import *
import gym
import cv2
import os
from functools import partial

GAME = 'Breakout-v0'
LOG_DIR = 'log/'+GAME
DISCOUNT_FACTOR = 0.99
DEVICE = '/gpu:0'
SAVE_PERIOD = 20000
SUMMARY_PERIOD = 100
LEARNING_RATE = 0.00025
DECAY = 0.99
GRAD_CLIP = 0.1
ENTROPY_BETA = 0.01
NUM_THREADS = 2
AGENT_PER_THREADS = 2
UNROLL_STEP = 5
MAX_ITERATION = 1000000
FRAMES_FOR_STATE = 4
IMAGE_H = 84
IMAGE_W = 84
RANDOM_SEED = 113
NUM_GPUS = 2
class ActorCritic():
    @staticmethod
    def _build_shared_block(state, scope_name):
        layers = state
        with tf.variable_scope(scope_name) as scope:
            layers = Conv2D(layers, out_channel=32, name_scope='conv0', kernel_shape=5)
            layers = MaxPooling(layers, size=2)
            layers = Conv2D(layers, out_channel=32, name_scope='conv1', kernel_shape=5)
            layers = MaxPooling(layers, size=2)
            layers = Conv2D(layers, out_channel=64, name_scope='conv2', kernel_shape=4)
            layers = MaxPooling(layers, size=2)
            layers = Conv2D(layers, out_channel=64, name_scope='conv3', kernel_shape=3)
            layers = MaxPooling(layers, size=2)
            layers = FullyConnected(layers, name_scope='fc0', out_dim=512)
            block = PReLU(layers, name_scope='prelu')
            return block, scope

    @staticmethod
    def _build_policy(block, nA, scope_name):
        with tf.variable_scope(scope_name):
            logits = FullyConnected(block, name_scope='fc_logit', out_dim=nA)
            policy = tf.nn.softmax(logits)
            log_policy = tf.log(policy + 1e-7)
            return policy, log_policy

    @staticmethod
    def _build_value(block, scope_name):
        with tf.variable_scope(scope_name):
            value = FullyConnected(block, name_scope='fc_v', out_dim=1)
            value = tf.squeeze(value, axis=1)
            return value

    def _sync_op(self, master):
        ops = [my.assign(master) for my, master in zip(self.train_vars, master.train_vars)]
        return tf.group(*ops)

    def __init__(self, nA, learning_rate, decay, grad_clip, entropy_beta,
                 state_shape=[IMAGE_H, IMAGE_W, FRAMES_FOR_STATE], master=None,
                 device_name='/gpu:0', scope_name='master'):
        self.nA = nA
        self.learning_rate = learning_rate
        self.decay = decay
        self.grad_clip = grad_clip
        self.entropy_beta = entropy_beta
        self.state_shape = state_shape
        self.master = master
        self.scope_name = scope_name
        self._build_train()

    def _build_train(self):
        with tf.Graph().as_default(), tf.device('/cpu:0'):
            for g_dev in range(NUM_GPUS):
                with tf.device('/gpu:%d' %g_dev):
                    self.state = tf.placeholder(tf.float32, shape=[None] + self.state_shape)
                    block, self.scope = ActorCritic._build_shared_block(self.state, self.scope_name+str(g_dev))
                    self.policy, self.log_softmax_policy = ActorCritic._build_policy(block, self.nA, self.scope_name+str(g_dev))
                    self.value = ActorCritic._build_value(block, self.scope_name+str(g_dev))

                    self.train_vars = sorted(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
                                                               self.scope.name), key=lambda v:v.name)
                    if self.master is not None:
                        self.sync_op = self._sync_op(self.master)
                        self.action = tf.placeholder(tf.int32, shape=[None,])
                        self.target_value = tf.placeholder(tf.float32, shape=[None,])

                        advantage = self.target_value - self.value
                        entropy = tf.reduce_sum(-1. * self.policy * self.log_softmax_policy, axis=1)
                        log_p_s_a = tf.reduce_sum(self.log_softmax_policy * tf.one_hot(self.action, self.nA), axis=1)
                        self.policy_loss = tf.reduce_mean(tf.stop_gradient(advantage)*log_p_s_a)
                        self.entropy_loss = tf.reduce_mean(entropy)
                        self.value_loss = tf.nn.l2_loss(advantage)

                        loss = -self.policy_loss - self.entropy_beta*self.entropy_loss + self.value_loss
                        self.gradients = tf.gradients(loss, self.train_vars)
                        clipped_gs = [tf.clip_by_average_norm(gr, self.grad_clip) for gr in self.gradients]
                        self.train_op = self.master.optimizer.apply_gradients(zip(clipped_gs, self.master.train_vars))
                    else:
                        self.optimizer = tf.train.RMSPropOptimizer(self.learning_rate, decay=self.decay, use_locking=True)

    def sync(self):
        if 'Thread' not in self.scope.name:
            return
        self.sess.run(self.sync_op)

    def initialize(self, sess):
        self.sess = sess
        if 'Thread' in self.scope.name:
            self.sync()

    def get_policy(self, s):
        return self.sess.run(self.policy, feed_dict={self.state: s})

    def get_value(self, s):
        return self.sess.run(self.value, feed_dict={self.state: s})

    def update(self, s, a, v):
        assert 'Thread' in self.scope.name
        policy_loss, entropy_loss, value_loss, _ = self.sess.run([self.policy_loss, self.entropy_loss,
                                                                  self.value_loss, self.train_op],
                                                                 feed_dict={self.state: s,
                                                                            self.action: a,
                                                                            self.target_value: v})
        return policy_loss, entropy_loss, value_loss

class HistoryBuffer():
    def __init__(self, preprocess_fn, image_shape, frames_for_state):
        self.buf = deque(maxlen=frames_for_state)
        self.preprocess_fn = preprocess_fn
        self.image_shape = image_shape
        self.clear()

    def clear(self):
        for i in range(self.buf.maxlen):
            self.buf.append(np.zeros(self.image_shape, np.float32))

    def add(self, obs):
        self.buf.append(self.preprocess_fn(obs))
        state = np.concatenate([img for img in self.buf], axis=2)
        return state

def _preprocess_obs(obs, image_size):
    y = 0.2126*obs[:,:,0] + 0.7152*obs[:,:,1] + 0.0722*obs[:,:,2]
    res = cv2.resize(y, image_size)
    res = res.astype(np.int8)
    return np.expand_dims(res.astype(np.float32), axis=2)

class A3CGroupAgent():
    def __init__(self, envs, actor_critic, unroll_step, discount_factor,
                 seed=None, image_size=(IMAGE_H, IMAGE_W), frames_for_state=FRAMES_FOR_STATE):
        self.envs = envs
        self.nA = envs[0].action_space.n
        self.observe_shape = list(image_size) + [1] #[84,84,1]
        self.frames_for_state = frames_for_state
        self.discount_factor = discount_factor
        self.preprocess = partial(_preprocess_obs, image_size=image_size)

        self.ac = actor_critic
        self.policy_func = actor_critic.get_policy
        self.value_func = actor_critic.get_value
        self.unroll_step = unroll_step

        self.random = np.random.RandomState(seed)
        self.hist_bufs = [HistoryBuffer(self.preprocess, self.observe_shape, frames_for_state) for _ in envs]
        self.states = [None for _ in envs]
        self.episode_rewards = [[] for _ in envs]
        self.episode_reward = [0. for _ in envs]

    def pick_action(self, s, greedy=False, epsilon=0.01):
        pi_given_s = self.policy_func(s)
        if greedy:
            if self.random.randn < epsilon:
                return [self.random.randint(0, self.nA)]
            else:
                return np.argmax(pi_given_s, axis=1)
        else:
            return [self.random.choice(self.nA, 1, p=p)[0] for p in pi_given_s]

    def enqueue_op(self, queue):
        def _func():
            for i, _ in enumerate(self.envs):
                if self.states[i] is None:
                    self.episode_rewards[i].append(self.episode_reward[i])
                    self.episode_reward[i] = 0.

                    self.hist_bufs[i].clear()
                    obs = self.envs[i].reset()
                    self.states[i] = self.hist_bufs[i].add(obs)

            done_envs = set()
            sras = [[] for _ in self.envs] # states rewards actions
            for step in range(self.unroll_step):
                actions = self.pick_action(np.stack(self.states, axis=0)) # Get actions for all agent enviroments
#                 print('hee')
                for i, (env, action) in enumerate(zip(self.envs, actions)):
#                     print('her')
                    if i in done_envs:
                        continue

                    obs, reward, done, _ = env.step(action)
                    self.episode_reward[i] += reward
#                     print('here3')
                    reward = max(-1.0, min(1.0, reward)) # clip reward -1 to 1
                    sras[i].append((self.states[i], reward, action))
                    self.states[i] = self.hist_bufs[i].add(obs)
#                     print('herhe2')
                    if done:
                        done_envs.add(i)

            vs = self.value_func(np.stack(self.states,axis=0)) # value estimate
            states = []
            actions = []
            values = []
            for i, sra in enumerate(sras):
                if i in done_envs:
                    vs[i] = 0.
                    self.states[i] = None

                for s,r,a in sra[::-1]: # reverse order
                    vs[i] = r + self.discount_factor*vs[i]
                    states.append(s)
                    actions.append(a)
                    values.append(vs[i])

            states = np.stack(states, axis=0)
            actions = np.stack(actions, axis=0).astype(np.int32)
            target_values = np.stack(values, axis=0).astype(np.float32)

            policy_loss, entropy_loss, value_loss = self.ac.update(states, actions, target_values)
            self.ac.sync()
            return policy_loss, entropy_loss, value_loss
        data = tf.py_func(_func, [], [tf.float32, tf.float32, tf.float32], stateful=True)
        return queue.enqueue(data)

    def num_episodes(self):
        return sum([len(l)-1 for l in self.episode_rewards if len(l) > 1])

    def reward_info(self):
        recent_rewards = [l[-1] for l in self.episode_rewards if len(l) > 1]
        avg_r = sum(recent_rewards) / len(recent_rewards) if len(recent_rewards) > 0 else float('nan')
        max_r = max(recent_rewards) if len(recent_rewards) > 0 else float('nan')
        return avg_r, max_r

def main():
    tf.set_random_seed(RANDOM_SEED)
    np.random.seed(RANDOM_SEED)
    tf.reset_default_graph()

    sample_env = gym.make(GAME)
    nA = sample_env.action_space.n
    global_step = tf.Variable(0, name='global_step', trainable=False)
    learning_rate = tf.train.polynomial_decay(LEARNING_RATE, global_step, MAX_ITERATION//2, LEARNING_RATE*0.1)

    master_ac = ActorCritic(nA, device_name=DEVICE, learning_rate=learning_rate,
                            decay=DECAY, grad_clip=GRAD_CLIP, entropy_beta=ENTROPY_BETA)

    group_agents = [A3CGroupAgent([gym.make(GAME) for _ in range(AGENT_PER_THREADS)],
                                  ActorCritic(nA, master=master_ac, device_name=DEVICE,
                                              scope_name='Thread%03d'%i, learning_rate=learning_rate,
                                              decay=DECAY, grad_clip=GRAD_CLIP,
                                              entropy_beta=ENTROPY_BETA),
                                  unroll_step=UNROLL_STEP, discount_factor=DISCOUNT_FACTOR,
                                  seed=i) for i in range(NUM_THREADS)]

    queue = tf.FIFOQueue(capacity=NUM_THREADS*10, dtypes=[tf.float32, tf.float32, tf.float32], )
    qr = tf.train.QueueRunner(queue, [agent.enqueue_op(queue) for agent in group_agents])
    tf.train.queue_runner.add_queue_runner(qr)
    loss = queue.dequeue()
    pl, el, vl = loss

    increase_step = global_step.assign(global_step+1)
    init_op = [tf.global_variables_initializer(), tf.local_variables_initializer()]

    def _train_info():
        total_eps = sum([agent.num_episodes() for agent in group_agents])
        avg_r = sum([agent.reward_info()[0] for agent in group_agents]) / len(group_agents)
        max_r = max([agent.reward_info()[1] for agent in group_agents])
        return total_eps, avg_r, max_r

    train_info = tf.py_func(_train_info, [], [tf.int64, tf.float64, tf.float64], stateful=True)
    total_eps, avg_r, max_r = train_info

    tf.summary.scalar('learning_rate', learning_rate)
    tf.summary.scalar('policy_loss', pl)
    tf.summary.scalar('entropy_loss', el)
    tf.summary.scalar('value_loss', vl)
    tf.summary.scalar('total_episodes', total_eps)
    tf.summary.scalar('average_rewards', avg_r)
    tf.summary.scalar('max_rewards', max_r)
    summary_op = tf.summary.merge_all()
#     config_summary = tf.summary.text('TrainConfig', tf.convert_to_tensor(config.as_maxtrix()), collections=[])

    saver = tf.train.Saver(var_list=master_ac.train_vars, max_to_keep=3)

    sess = tf.InteractiveSession()
    sess.graph.finalize()

    if os.path.isfile(LOG_DIR+'/model.ckpt.index'):
        saver.restore(sess, LOG_DIR+'/model.ckpt') # Load model if exists
    elif os.path.isfile(LOG_DIR+'/last.ckpt.index'):
        sess.close()
        return # If already done training --> quit
    else:
        sess.run(init_op)

    master_ac.initialize(sess)
    for agent in group_agents:
        agent.ac.initialize(sess)
    print('Complete Initialization...')

    try:
        summary_writer = tf.summary.FileWriter(LOG_DIR, sess.graph)
        summary_writer_eps = tf.summary.FileWriter(os.path.join(LOG_DIR,'per-eps'))

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess, coord=coord)
        for step in tqdm(range(MAX_ITERATION)):
            if coord.should_stop():
                break

            (pl, el, vl), summary_str, (total_eps, avg_r, max_r), _ = sess.run([loss, summary_op,
                                                                                train_info,
                                                                                increase_step])
            if step % SUMMARY_PERIOD == 0:
                summary_writer.add_summary(summary_str, step)
                summary_writer_eps.add_summary(summary_str, total_eps)
                tqdm.write('step(%7d) policy_loss:%1.5f,entropy_loss:%1.5f,value_loss:%1.5f, te:%5d avg_r:%2.1f max_r:%2.1f'%
                        (step,pl,el,vl,total_eps,avg_r,max_r))

            if (step+1) % SAVE_PERIOD == 0:
                saver.save(sess, LOG_DIR+'/Model/model.ckpt', global_step=step+1)
                break
    except Exception as e:
        coord.request_stop(e)
    finally:
        coord.request_stop()
        coord.join(threads)
        saver.save(sess, LOG_DIR+'/Model/last.ckpt')
        sess.close()

main()
