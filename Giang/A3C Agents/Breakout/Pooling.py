import tensorflow as tf

def MaxPooling(x, size, stride=None, padding='VALID', data_format='NHWC'):
    if stride is None:
        stride = size

    if data_format == 'NHWC':
        channels_pos = 'channels_last'
    else:
        channels_pos = 'channels_first'
    pool_out = tf.layers.max_pooling2d(x, size, stride, padding, channels_pos)
    return tf.identity(pool_out, name='pool_out')
