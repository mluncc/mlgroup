import tensorflow as tf
import numpy as np

def Flatten(x):
    shape = x.get_shape().as_list()[1:]
    if None not in shape:
        return tf.reshape(x, [-1, int(np.prod(shape))])
    return tf.reshape(x, tf.stack([tf.shape(x)[0], -1]))

def FullyConnected(x, out_dim, name_scope, W_init=None, b_init=None,
                   activation_fn=tf.identity, use_bias=True):
    x = Flatten(x)
    if W_init is None:
        W_init = tf.contrib.layers.variance_scaling_initializer()
    if b_init is None and use_bias:
        b_init = tf.constant_initializer()
    W_shape = [x.shape[-1], out_dim]

    with tf.variable_scope(name_scope):
        W = tf.get_variable('W', W_shape, initializer=W_init)
        if use_bias:
            b = tf.get_variable('b', [out_dim], initializer=b_init)

        fc_out = activation_fn(tf.add(tf.matmul(x,W), b), name='fc_out')

    return fc_out
