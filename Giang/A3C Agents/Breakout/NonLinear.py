import tensorflow as tf

def PReLU(x, name_scope, init=0.001, name='PReLU_out'):
    with tf.variable_scope(name_scope):
        init = tf.constant_initializer(init)
        alpha = tf.get_variable('alpha', [], initializer=init)
        x = ((1 + alpha) * x + (1 - alpha) * tf.abs(x))
        PReLU_out = tf.multiply(x, 0.5, name=name)
    return PReLU_out
