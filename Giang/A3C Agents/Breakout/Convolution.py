import tensorflow as tf

def _get_kernel_shape(kernel_shape):
    if type(kernel_shape) == int:
        kernel_shape = [kernel_shape, kernel_shape]
    else:
        kernel_shape = list(kernel_shape)
    return kernel_shape

def _get_stride(stride, data_format):
    if type(stride) == int:
        stride = [stride, stride]
    else:
        stride = list(stride)

    if data_format == 'NHWC':
        stride = [1] + stride + [1] # Appending 1 in the beginning and ending
    else:
        stride = [1, 1] + stride # Appending 1, 1 at the beginning
    return stride

def Conv2D(x, out_channel, name_scope,
        kernel_shape, padding='SAME', stride=1,
        W_init=None, b_init=None, activation_fn=tf.nn.relu,
        split=1, use_bias=True, data_format='NHWC'):
    in_shape = x.get_shape().as_list()
    if data_format == 'NHWC':
        channel_axis = 3 # NHWC -- channel is last (4)
    else:
        channel_axis = 1 # channel is second
    in_channel = in_shape[channel_axis] # number of channels

    kernel_shape = _get_kernel_shape(kernel_shape)
    padding = padding.upper()
    filter_shape = kernel_shape + [in_channel/split, out_channel]
    stride = _get_stride(stride, data_format)

    if W_init is None:
        W_init = tf.contrib.layers.variance_scaling_initializer()
    if b_init is None and use_bias:
        b_init = tf.constant_initializer()

    with tf.variable_scope(name_scope):
        W = tf.get_variable('W', filter_shape, initializer=W_init)
        if use_bias:
            b = tf.get_variable('b', [out_channel], initializer=b_init)

        if split == 1:
            conv = tf.nn.conv2d(x, W, stride, padding, data_format=data_format)
        else:
            inputs = tf.split(x, split, channel_axis)
            kernels = tf.split(W, split, 3)
            outputs = []
            for i, j in zip(inputs, kernels):
                output.append(tf.nn.conv2d(i, k, stride, padding, data_format=data_format))
            conv = tf.concat(outputs, channel_axis)

        if use_bias:
            conv_out = activation_fn(tf.nn.bias_add(conv, b, data_format=data_format), name='conv_out')
        else:
            conv_out = activation_fn(conv, name='conv_out')

    return conv_out
